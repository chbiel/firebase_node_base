# exit when any command fails
set -e

###############################################################################
# Execution environment
###############################################################################
export TMPDIR=/private$TMPDIR
export DEBUG=0

###############################################################################
# Helper functions
###############################################################################

usage() {
  echo "Usage:"
  echo -n "$0 "
  set | grep -e "^task_" | sed "s/^task_\(.*\)().*/\1/" | xargs | sed "s/ / | /g"
  exit 1
}

###############################################################################
# Dependency
###############################################################################

task_install_dependencies() {
  npm install
  npm run install:frontend
  npm run install:functions
}

###############################################################################
# Build
###############################################################################

task_build() {
  local env=$1

  if [[ -z $env ]]
  then 
    echo "error: missing environment"
    exit 1
  fi 

  npm run build:frontend
  npm run build:functions
}

###############################################################################
# Clean
###############################################################################

task_clean() {
  echo "hello clean up"
}

###############################################################################
# Run
###############################################################################

task_run() {
  echo "run"
}


###############################################################################
# Test
###############################################################################
task_test_unit_frontend() {
  echo "test frontend"
}

task_test_unit_backend() {
 echo "test backend"
}

task_test_unit() {
   task_test_unit_frontend
   task_test_unit_backend
}

###############################################################################
# Database
###############################################################################
task_database_create_table() {
    echo "Hello create table"
}

task_database_seed_table() {
    echo "Hello seed table"
}

task_database_delete_table() {
  echo "Hello delete table"
}

###############################################################################
# Deploy
###############################################################################
task_deploy() {
  local env=$1

  if [[ -z $env ]]
  then 
    echo "error: missing environment"
    exit 1
  fi 
  
  ./node_modules/firebase-tools/lib/bin/firebase.js deploy --project=$env
} 

###############################################################################
# dependencies, test, build & deploy to dev
###############################################################################

task_test_build_deploy() {
  local env=$1

  if [[ -z $env ]]
  then 
    echo "error: missing environment"
    exit 1
  fi

  task_install_dependencies
  task_test_unit
  task_test_integration
  task_test_functional
  task_build
  task_deploy $env
}

###############################################################################
# Docker
###############################################################################

main() {
  CMD=${1:-}
  shift || true
  if type "task_${CMD}" &> /dev/null; then
    "task_${CMD}" "$@"
  else
    usage
  fi
}

main "$@"
